#!/usr/bin/env python
from setuptools import setup, find_packages
setup(name='LeR',
      version='0.1',
      description='LEnsing Rates',
      author='Hemantakumar',
      license="MIT",
      author_email='hemantaphurailatpam@gmail.com',
      url='https://git.ligo.org/hemantakumar.phurailatpam/ler',
      packages=find_packages(),
      install_requires=[
        "setuptools>=61.1.0",
        "bilby>=1.0.2",
        "pycbc>=2.0.4",
        "quintet>=0.1"
      ]
     )
