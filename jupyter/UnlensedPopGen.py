#!/home/narola.bharatbhai/.conda/envs/lensing-ET/bin/python
"""
Upgrade of the `NotOnMyCpu.py` file. This version 
of the file will compute the SNR for complete events
(with HOM and precession). However, since it is heavier
we will need to run it in parallel batches
and put the events together afterwards
"""
import numpy as np
from scipy.integrate import simps
from scipy.stats import betaprime, uniform, randint
from scipy.special import erf, erfinv
import time
from mass_sampling import mass_distribution
import bilby
from astropy.cosmology import FlatLambdaCDM
import argparse

# set the default cosmology 

parser = argparse.ArgumentParser(description = 'Code to generate the unlensed population')
parser.add_argument('-batch_nbr', '--batch_nbr', type = int, default = 0, help = 'Number of the batch to be run')
parser.add_argument('-batch_size', '--batch_size', type = int, default = 50000, help = 'Number of events that should be created in the batch')
parser.add_argument('-precession', '--precession', type = bool, default = True, help = 'Whether precession should be considered or not')
parser.add_argument('-zmax', '--zmax', type = int, default = 15, help = 'Maximum frequency to consider in the run')
parser.add_argument('-path', '--path', type = str, default = '.', help = 'Run path for the batch')
parser.add_argument('-label', '--label', type = str, default = None, help = 'label for the batch')

parser.add_argument('-seed', '--seed', type = int, default = None, help = 'Seed for the batch, can be used for reproduction')

args = parser.parse_args()

# read in the arguments 
batch = args.batch_nbr
batch_size = args.batch_size
precession = args.precession
zmax = args.zmax
seed = args.seed
path = args.path

####################################################
#                                                  #
#   ARGUMENTS TO SETUP MANUALLY BEFORE THE RUN     #
#                                                  #
####################################################
cosmo = FlatLambdaCDM(H0 = 70, Om0 = 0.31)

list_of_detectors = ['ET1', 'ET2', 'ET3', 'CE_40', 'CE_20']
flow = 5.
sampling_frequency = 4096.
approximant = 'IMRPhenomXPHM'
psds = dict()
psds['L1'] = 'aLIGO_O4_high_asd.txt'
psds['H1'] = 'aLIGO_O4_high_asd.txt'
psds['V1'] = 'AdV_asd.txt'
psds['K1'] = 'KAGRA_design_asd.txt'
psds['a_1'] = 'aLIGO_O4_high_asd.txt'
psd_file = True # set to false if file is an ASD file




####################################################
#                                                  #
#          USEFUL FUNCTION FOR THE RUN             #
#                                                  #
#################################################### 

def dVcdzCoefficient(zmax = 15):
    
    """The normalisation factor of the redshift distribution"""
    
    ztot = np.linspace(0.01, zmax, num = 1000)
    dVctot = 4*np.pi*cosmo.differential_comoving_volume(ztot)/1E9
    norm1 = simps(dVctot, ztot)
    
    return norm1

def LuminosityDistance(redshift):
    
    dL = cosmo.luminosity_distance(redshift).value
    
    return dL

def MergerRateDensity(z, R0 = 23.9, a2 = 1.6, a3 = 2.0, a4 = 30):
    
    """
    Semi-analytical fit to the BBH merger rate density from Belczynski+ (2016),
    inspired by Oguri (2018).
        
    It's re-scaled to fit R0 = 23.9 +14.9/-8.0 from the R&P paper
    """
    
    a1 = R0*(1 + a4)
    
    R = (a1*np.exp(a2*z))/(np.exp(a3*z) + a4)
    
    return R

def SmoothingFunction(m, m_min, deltam):
    
    """Smoothing function used for the mass distributions"""
    
    f = deltam/(m - m_min) + deltam/(m - m_min - deltam)
    
    if m <= m_min:
        S = 0
    elif m >= m_min + deltam:
        S = 1
    else:
        #print(f)
        S = (np.exp(f) + 1)**(-1)
    
    return S


def InclinationPPF(u):
    
    """For sampling the inclination"""
    
    ppf = np.arccos(1 - 2*u)
    
    return ppf


def TruncPowerLawPPF(u, alpha, m_min, m_max):
    
    """For sampling the m1 and m2 distributions"""
    
    ppf = (u*(m_max**(1 + alpha) - m_min**(1 + alpha)) \
           + m_min**(1 + alpha))**(1/(1 + alpha))
    
    return ppf


def GaussianPPF(u, mu_m, sigma_m):
    
    """For sampling the m1 distribution"""
    
    ppf = mu_m + sigma_m*2**0.5*erfinv(2*u - 1)
    
    return ppf


def LogNormIshPPF(u, a = 1.1375, b = 0.8665, zmax = 15):
    
    """For sampling the analytical approximation of the redshift distribution"""
    
    ppf = np.exp(a**2 + b - a*2**0.5*erfinv(1 - u*(1 - erf((a**2 + b - 
                                                np.log(zmax))/2**0.5/a))))
    
    return ppf


def BetaprimePPF(u, a = 2.906, b = 0.0158, c = 0.58, zmax = 15):
    
    """For sampling the analytical approximation of the redshift distribution"""
    
    ppf = betaprime.ppf(u*betaprime.cdf(zmax, a, b, loc = c), a, b, loc = c)
    
    return ppf


def RedshiftSampler(lambda_z = 0.563, a1 = 2.906, b1 = 0.0158, c = 0.58, 
                    a2 = 1.1375, b2 = 0.8665, zmax = 15):
    
    """
    Function for sampling the redshift distribution using a 
    rejection sampling procedure.
    """

    while(True):
        
        # Random number between 0 and 1 that will define which
        # distribution will be drawn from
        u = uniform.rvs()
        
        if u >= lambda_z:
            sample = BetaprimePPF(uniform.rvs(), a = a1, b = b1, c = c, 
                                  zmax = zmax)
            return sample
            break
        
        else:
            sample = LogNormIshPPF(uniform.rvs(), a = a2, b = b2, zmax = zmax)
            return sample
            break


def m1Sampler(lambda_m = 0.10, alpha = -2.63, m_min = 4.59, m_max = 86.22, 
              deltam = 4.82, mu_m = 33.07, sigma_m = 5.69):
    
    """
    Function for sampling the m1 distribution using a 
    rejection sampling procedure
    """

    while(True):
    
        # Decide which distribution to draw from
        u1 = uniform.rvs()
    
        if u1 >= lambda_m:
    
            u2 = uniform.rvs()
            sample = TruncPowerLawPPF(uniform.rvs(), alpha, 1, m_max)
            
            # This will force the resulting distribution
            # to follow the smoothing function where necessary.
            if u2 < SmoothingFunction(sample, m_min, deltam): 
                return sample
                break
        
        # Same procedure, different distribution
        else: 
            u2 = uniform.rvs()
            sample = GaussianPPF(uniform.rvs(), mu_m, sigma_m)
            
            if u2 < SmoothingFunction(sample, m_min, deltam):
                return sample
                break
            

def qSampler(m1, beta = 1.26, m_min = 4.59, deltam = 4.82):
    
    "Function for sampling the q = m2/m1 distribution using a rejection sampling procedure"
    
    # Same principle as for the m1 sampler, 
    # but there's just one distribution to draw from
    while(True):
    
        u = uniform.rvs()
        sample = TruncPowerLawPPF(uniform.rvs(), beta, m_min/m1, 1)
    
        if u <= SmoothingFunction(sample, m_min/m1, deltam/m1):
            return sample
            break


####################################################
#                                                  #
#             CORE OF THE ALGORITHM                #
#                                                  #
#################################################### 

if seed is not None:
    np.random.seed(seed) # freeze everything for reproduction

# start time
startSNR = time.time()

# useful quantities for later
cumulative = 0
coefficient = dVcdzCoefficient(zmax = zmax)

# waveform arguments to be used by bilby 
waveform_arguments = dict(waveform_approximant = approximant,
                          reference_frequency = 50.,
                          minimum_frequency = flow)

# intiate file to write the results
file = open('%s/results_%s_%iSample_batch%i.txt'%(args.path, args.label, batch_size, batch), 'w')
file.write('#Results from rejection sampling run with {:.0f} samples\n'.format(batch_size))
det_str = " "
for i in range(len(list_of_detectors)):
    det_str += "hp_inner_hp_%s \t hc_inner_hc_%s \t hp_inner_hc_%s \t"%(list_of_detectors[i], list_of_detectors[i], list_of_detectors[i])
if approximant in ['IMRPhenomD', 'IMRPhenomXHM']: # add other aligned spins waveform if needed
    spin_str = "chi_1 \t chi2"
else:
    spin_str = "a_1 \t a_2 \t tilt_1 \t tilt_2 \t phi_12 \t phi_jl \t"
file.write('#m1 \t m2 \t %s  NetSNR \t redshift \t iota \t psi \t alpha \t delta \t GPStime \t phase \t %s\n'%(det_str, spin_str))

# make the powerlax + peak prior
PP_pars = {'alpha': 3.63,
  'beta': 1.26,
  'delta_m': 4.82,
  'mmin': 4.59,
  'mmax': 86.22,
  'lam': 0.08,
  'mpp': 33.07,
  'sigpp': 5.69}
md = mass_distribution(**PP_pars)

# load psds once and for all as useful objects
psds_arrays = dict()
if psd_file:
    for key in psds:
        psds_arrays[key] = bilby.gw.detector.PowerSpectralDensity(psd_file = psds[key])
else:
    for key in psds:
        psds_arrays[key] = blby.gw.detector.PowerSpectralDensity(asd_file = psds[key])

# make the ifos as bilby ifo's object to be used later
ifos_objects = dict()
for key in list_of_detectors:
    ifos_objects[key] = bilby.gw.detector.networks.get_empty_interferometer(key)


# make the events and compute their information 
for i in range(batch_size):
    if i%1000 == 0:
        print("Generation %i / %i"%(i, batch_size))
    # draw redshift and convert to luminosity distance
    redshiftValue = np.array(RedshiftSampler(zmax = 10))
    dLValue = LuminosityDistance(redshiftValue)
    
    #Draw the binary masses and compute the SNR
    m1Value, m2Value = np.ravel(md.sample(1).T)
    m1ValueDet = m1Value*(1 + redshiftValue)
    m2ValueDet = m2Value*(1 + redshiftValue)

    # draw associated angles 
    rightAscensionValue = uniform.rvs(scale = 2*np.pi)
    declinationValue = np.arcsin(2*uniform.rvs() - 1)
    polarisationValue = uniform.rvs(scale = 2*np.pi)
    inclinationValue = InclinationPPF(uniform.rvs())
    phaseValue = uniform.rvs(scale = 2*np.pi)

    # compute GPS time
    GPStimeValue = randint.rvs(0, 31.6E6)

    # take the spin values (depends on precession or not)
    if approximant in ['IMRPhenomD', 'IMRPhenomD']:
        chi1Value = uniform.rvs(scale = 1)
        chi2Value = uniform.rvs(scale = 1)

        # make parameter dictionary
        parameters = dict(mass_1 = m1ValueDet, mass_2 = m2ValueDet,
                          chi_1 = chi1Value, chi_2 = chi2Value,
                          luminosity_distance = dLValue,
                          ra = rightAscensionValue,
                          dec = declinationValue,
                          psi = polarisationValue,
                          theta_jn = inclinationValue,
                          phase = phaseValue,
                          geocent_time = GPStimeValue)
    else:
        if precession:
            # need to account for spin
            a1Value = uniform.rvs(scale = 1)
            a2Value = uniform.rvs(scale = 1)
            tilt1Value = np.arcsin(2*uniform.rvs() - 1)
            tilt2Value = np.arcsin(2*uniform.rvs() - 1)
            phi12Value = uniform.rvs(scale = 2*np.pi)
            phijlValue = uniform.rvs(scale = 2*np.pi)

        else:
            # assume aligned spins with only an amplitude
            a1Value = uniform.rvs(scale = 1)
            a2Value = uniform.rvs(scale = 1)
            tilt1Value = 0
            tilt2Value = 0
            phi12Value = 0
            phijlValue = 0


        parameters = dict(mass_1 = m1ValueDet, mass_2 = m2ValueDet,
                          a_1 = a1Value, a_2 = a2Value,
                          tilt_1 = tilt1Value, tilt_2 = tilt2Value,
                          phi_12 = phi12Value, phi_jl = phijlValue,
                          luminosity_distance = dLValue,
                          ra = rightAscensionValue,
                          dec = declinationValue,
                          psi = polarisationValue,
                          theta_jn = inclinationValue,
                          phase = phaseValue,
                          geocent_time = GPStimeValue)
    # now we can setup the waveform generator to 
    # make the two polarizations
    # first need to have an approximate signal duration
    approx_duration = bilby.gw.utils.calculate_time_to_merger(waveform_arguments['minimum_frequency'],
                                                              parameters['mass_1'],
                                                              parameters['mass_2'],
                                                              safety = 1.2)
    duration = np.ceil(approx_duration + 4.)

    waveform_generator = bilby.gw.WaveformGenerator(duration = duration,
                                                    sampling_frequency = sampling_frequency,
                                                    frequency_domain_source_model = bilby.gw.source.lal_binary_black_hole,
                                                    waveform_arguments = waveform_arguments)
    
    polas = waveform_generator.frequency_domain_strain(parameters = parameters)
    done_ifos = []
    done_psds = []
    hp_inner_hp = dict()
    hp_inner_hc = dict()
    hc_inner_hc = dict()
    snrs_sq = dict()
    NetSNR = 0.
    for ifo in list_of_detectors:
        if psds[ifo] in done_psds:
            # already computed the inner product for 
            # the corresponsing PSD
            seen_ifo = done_ifos[done_psds.index(psds[ifo])]
            hp_inner_hp[ifo] = hp_inner_hp[seen_ifo]
            hp_inner_hc[ifo] = hp_inner_hc[seen_ifo]
            hc_inner_hc[ifo] = hc_inner_hc[seen_ifo]
        else:
            # need to compute the inner product for
            # the firs time
            p_array = psds_arrays[ifo].get_power_spectral_density_array(waveform_generator.frequency_array)
            hp_inner_hp[ifo] = bilby.gw.utils.noise_weighted_inner_product(polas['plus'],
                                                                           polas['plus'],
                                                                           p_array,
                                                                           waveform_generator.duration)
            hp_inner_hc[ifo] = bilby.gw.utils.noise_weighted_inner_product(polas['plus'],
                                                                           polas['cross'],
                                                                           p_array,
                                                                           waveform_generator.duration)
            hc_inner_hc[ifo] = bilby.gw.utils.noise_weighted_inner_product(polas['cross'],
                                                                           polas['cross'],
                                                                           p_array,
                                                                           waveform_generator.duration)
            done_ifos.append(ifo)
            done_psds.append(psds[ifo])

        # make an ifo object to get the antenna pattern
        Fpl = ifos_objects[ifo].antenna_response(parameters['ra'], parameters['dec'],
                                                 parameters['geocent_time'],
                                                 parameters['psi'], 'plus')
        Fcr = ifos_objects[ifo].antenna_response(parameters['ra'], parameters['dec'],
                                                 parameters['geocent_time'],
                                                 parameters['psi'], 'cross')
        snrs_sq[ifo] = (Fpl**2)*hp_inner_hp[ifo] + (Fcr**2)*hc_inner_hc[ifo] \
                             + 2*Fpl*Fcr*hp_inner_hc[ifo]
        NetSNR += snrs_sq[ifo]

    NetSNR = np.sqrt(NetSNR)

    inners_out = " "
    for ifo in list_of_detectors:
        inners_out += "%.5f \t %.5f \t %.5f \t"%(hp_inner_hp[ifo], hc_inner_hc[ifo], hp_inner_hc[ifo])

    if approximant in ['IMRPhenomD', 'IMRPhenomXHM']:
        spins_out = "%.3f \t %.3f "%(parameters['chi_1'], parameters['chi_2'])
    else:
        spins_out = "%.3f \t %.3f \t %.3f \t %.3f \t %.3f \t %.3f "%(parameters['a_1'], parameters['a_2'],
                                                                               parameters['tilt_1'], parameters['tilt_2'],
                                                                               parameters['phi_12'], parameters['phi_jl'])

    file.write('%.3f \t %.3f \t %s %.3f \t %.3f \t %.3f \t %.3f \t %.3f \t %.3f \t %.3f \t %.3f \t %s \n'%(parameters['mass_1'],
                parameters['mass_2'], inners_out, NetSNR, redshiftValue, parameters['theta_jn'], parameters['psi'], parameters['ra'],
                parameters['dec'], parameters['geocent_time'], parameters['phase'], spins_out))

    # account for the merger rate if detected
    if NetSNR < 8:
        continue

    mergerRateValue = MergerRateDensity(redshiftValue)
    cumulative += mergerRateValue/(1 + redshiftValue)

# compute the actual observed rate 
observedRates = coefficient*cumulative/batch_size
file.write('#Observed rate for the detector network: %.5e\n'%(observedRates))
file.write('#Computation time: %.3f s'%(time.time() - startSNR))
file.close()
